$(function() {
	$('.btn, .btn-reset').on('click', function() { 
		var _this = $(this),
			_counter = _this.closest('.container').find('.displayCount'),
			_val = _counter.text();

			_this.hasClass('btn') ? _counter.html(++_val) : _counter.html(0);
	}); 	

});

